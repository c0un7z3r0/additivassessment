// WEB BUILD Entry point
// Used for both 'dev' mode and 'production' mode builds
import React from 'react'
import { render } from 'react-dom'

// APPLICATION ROOT COMPONENT
import App from './app'

// REACT ROUTER DOM
import { BrowserRouter } from 'react-router-dom'

render(
  <BrowserRouter basename={'/'}>
    <App />
  </BrowserRouter>,
  document.getElementById('application')
)
