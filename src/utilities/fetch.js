export const fetchEmployee = async (name) => {
  let url = `https://api.additivasia.io/api/v1/assignment/employees/${name}`
  let req = await fetch(url)
  if (req.status === 200) {
    let json = await req.json()
    return json
  } else {
    new Error({ error: req.statusText })
  }
}

export const recursiveFetchEmployees = async (initialName, uniqueNameArray = []) => {
  if (!uniqueNameArray.includes(initialName)) {
    uniqueNameArray.push(initialName)
    let json = await fetchEmployee(initialName)
    const role = json[0]
    const subordinates = json[1]
    if (subordinates) {
      return {
        name: initialName,
        role: role,
        subordinates: await Promise.all(
          subordinates['direct-subordinates'].map((subordinate) =>
            recursiveFetchEmployees(subordinate, uniqueNameArray)
          )
        ),
      }
    } else {
      return {
        name: initialName,
        role: role,
      }
    }
  }
}
