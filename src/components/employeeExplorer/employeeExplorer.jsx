import React, { useEffect, useState } from 'react'
import { useHistory, useLocation, useParams } from 'react-router-dom'
import { recursiveFetchEmployees } from '../../utilities/fetch'
import LoadingSpinner from '@perpetualsummer/catalyst-elements/LoadingSpinner'
import HomeScreen from '../home/home'
import Employee from './employee/employee'
import classNames from 'classnames/bind'
import styles from './employeeExplorer.module'

let cx = classNames.bind(styles)

const EmployeeExplorer = () => {
  const [searchInputValue, setSearchInputValue] = useState('')
  const [employeeHierarchy, setEmployeeHierarcy] = useState(null)
  const [loading, setLoading] = useState(false)
  const [error, setError] = useState(false)

  const history = useHistory()
  const location = useLocation()
  const { pathname } = location
  const { employee: employeeFromUrl = '' } = useParams()

  // Get Name from URL if possible
  useEffect(() => {
    if (pathname.includes('/overview/')) {
      if (pathname === '/overview/') {
        history.push(`/`, { from: pathname })
      }
      setLoading(true)
      setSearchInputValue(employeeFromUrl)
      recursiveFetchEmployees(employeeFromUrl)
        .then((employees) => {
          setError(false)
          setLoading(false)
          setEmployeeHierarcy(employees)
        })
        .catch(() => {
          setError(true)
          setLoading(false)
          setEmployeeHierarcy([])
        })
    }
    if (pathname === '/') {
      setSearchInputValue('')
    }
  }, [history, pathname, setSearchInputValue, employeeFromUrl])

  // Push Name to URL if using search bar
  const submitSearch = () => {
    history.push(`/overview/${searchInputValue}`, { from: pathname })
  }

  let employeeContainerClasses = cx({
    employeeContainer: true,
    employeeContainerHome: pathname === '/',
  })

  const formStatus = () => {
    if (employeeHierarchy === null) {
      return 'ready'
    }
    if (!employeeHierarchy.role) {
      return 'no-results'
    }
    if (employeeHierarchy.role) {
      return 'results'
    }
  }

  return (
    <main className={styles.mainContainer}>
      <div className={pathname === '/' ? styles.searchContainerHome : styles.searchContainer}>
        <HomeScreen
          setEmployeeName={setSearchInputValue}
          employeeName={searchInputValue}
          submitEmployee={() => submitSearch()}
          formStatus={formStatus()}
          hasError={error}
        />
      </div>
      <div className={employeeContainerClasses}>
        {loading ? (
          <LoadingSpinner />
        ) : (
          employeeHierarchy &&
          employeeHierarchy.role && (
            <ul className={styles.employeeList}>
              <Employee
                name={employeeHierarchy.name}
                role={employeeHierarchy.role}
                subordinates={employeeHierarchy.subordinates}
              />
            </ul>
          )
        )}
      </div>
    </main>
  )
}

export default EmployeeExplorer
