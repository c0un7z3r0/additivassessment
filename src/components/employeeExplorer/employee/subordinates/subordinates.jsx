import React from 'react'
import propTypes from 'prop-types'
import Employee from '../employee'

const Subordinates = ({ subordinates }) => {
  return (
    <ul>
      {subordinates.map((subordinate) => {
        if (subordinate !== undefined) {
          const { name, role, subordinates } = subordinate
          return <Employee key={name} name={name} role={role} subordinates={subordinates} />
        }
      })}
    </ul>
  )
}
Subordinates.propTypes = {
  subordinates: propTypes.array,
}

export default Subordinates
