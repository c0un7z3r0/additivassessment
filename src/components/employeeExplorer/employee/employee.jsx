import React from 'react'
import propTypes from 'prop-types'
import { useHistory, useLocation } from 'react-router-dom'
import Subordinates from './subordinates/subordinates'
import classNames from 'classnames/bind'
import styles from './employee.module'

let cx = classNames.bind(styles)

const Employee = ({ name, role, subordinates }) => {
  const history = useHistory()
  const location = useLocation()
  const { pathname } = location

  let employeeRoleClasses = cx({
    employeeRole_CEO: role === 'CEO',
    employeeRole_Supervisor: role.includes('supervisor') === true,
    employeeRole_Employee: role === 'employee',
  })

  const submitSearch = () => {
    history.push(`/overview/${name}`, { from: pathname })
  }

  return (
    <li className={styles.employeeListItem}>
      <div className={styles.employeeNameRoleContainer}>
        <span className={styles.employeeName}>{name}</span>
        <span className={styles.roleSearchContainer}>
          <span className={employeeRoleClasses}>{role}</span>
          <span className={styles.searchIcon} onClick={() => submitSearch()}>
            <svg xmlns="http://www.w3.org/2000/svg" width="25" height="25" viewBox="0 0 24 24">
              <path fill="none" d="M0 0h24v24H0V0z"></path>
              <path d="M15.5 14h-.79l-.28-.27A6.471 6.471 0 0016 9.5 6.5 6.5 0 109.5 16c1.61 0 3.09-.59 4.23-1.57l.27.28v.79l5 4.99L20.49 19l-4.99-5zm-6 0C7.01 14 5 11.99 5 9.5S7.01 5 9.5 5 14 7.01 14 9.5 11.99 14 9.5 14z"></path>
            </svg>
          </span>
        </span>
      </div>
      {subordinates && subordinates.length > 0 && <Subordinates subordinates={subordinates} />}
    </li>
  )
}
Employee.propTypes = {
  name: propTypes.string,
  role: propTypes.string,
  subordinates: propTypes.array,
}

export default Employee
