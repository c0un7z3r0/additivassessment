import React from 'react'
import propTypes from 'prop-types'
import InputText from '@perpetualsummer/catalyst-elements/InputText'
import Button from '@perpetualsummer/catalyst-elements/Button'
import styles from './home.module'

const HomeScreen = ({ setEmployeeName, employeeName, submitEmployee, formStatus, hasError }) => {
  const formSubmit = (e) => {
    e.preventDefault()
    submitEmployee()
  }

  return (
    <div className={styles.homeContainer}>
      <h3 className={styles.searchHeading}>
        <strong>Employee Explorer</strong>
      </h3>
      <div className={styles.formContainer}>
        <form onSubmit={(e) => formSubmit(e)}>
          <InputText
            id="employee_name_input"
            label="Employee Name"
            onChange={(e) => setEmployeeName(e.target.value)}
            value={employeeName}
            warning={formStatus === 'no-results'}
            valid={formStatus === 'results'}
          />
          <Button
            type="submit"
            id="employee_name_submit"
            onClick={(e) => formSubmit(e)}
            className={styles.submitButton}
          >
            SEARCH
          </Button>
        </form>
      </div>
      {hasError ? <p>There was an error with this search, please try again</p> : <p>&nbsp;</p>}
    </div>
  )
}
HomeScreen.propTypes = {
  setEmployeeName: propTypes.func.isRequired,
  employeeName: propTypes.string,
  submitEmployee: propTypes.func.isRequired,
  formStatus: propTypes.string,
  hasError: propTypes.bool,
}

export default HomeScreen
