import React from 'react'
import { Route, Link } from 'react-router-dom'
import EmployeeExplorer from '../employeeExplorer/employeeExplorer'
import AdditivLogo from '../../public/images/additiv_logo.png'
import ACregan from '../../public/images/Anon.jpg'
import styles from './rootView.module'

const RootView = () => {
  return (
    <article className={styles.rootView}>
      <header className={styles.header}>
        <div className={styles.logo}>
          <img src={AdditivLogo} />
          <span>Candidate Assessment</span>
        </div>
        <div className={styles.candidate}>
          <img src={ACregan} />
          <h5>Anthony Cregan</h5>
        </div>
      </header>
      <Route exact path={['/', '/overview/', '/overview/:employee']}>
        <EmployeeExplorer />
      </Route>
      <footer className={styles.footer}>
        <div className={styles.email}>
          <div className={styles.emailIcon}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="44"
              height="44"
              fill="whitesmoke"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0h24v24H0V0z"></path>
              <path d="M22 6c0-1.1-.9-2-2-2H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6zm-2 0l-8 5-8-5h16zm0 12H4V8l8 5 8-5v10z"></path>
            </svg>
          </div>
          <div className={styles.emailLink}>
            <Link
              to="#"
              onClick={(e) => {
                e.preventDefault()
                window.location = 'mailto:hello@anthonycregan.co.uk'
              }}
            >
              Hello
              <wbr />
              @AnthonyCregan.co.uk
            </Link>
          </div>
        </div>
        <div className={styles.telephone}>
          <div className={styles.telephoneIcon}>
            <svg
              xmlns="http://www.w3.org/2000/svg"
              width="44"
              height="44"
              fill="whitesmoke"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0h24v24H0V0z"></path>
              <path d="M6.54 5c.06.89.21 1.76.45 2.59l-1.2 1.2c-.41-1.2-.67-2.47-.76-3.79h1.51m9.86 12.02c.85.24 1.72.39 2.6.45v1.49c-1.32-.09-2.59-.35-3.8-.75l1.2-1.19M7.5 3H4c-.55 0-1 .45-1 1 0 9.39 7.61 17 17 17 .55 0 1-.45 1-1v-3.49c0-.55-.45-1-1-1-1.24 0-2.45-.2-3.57-.57a.84.84 0 00-.31-.05c-.26 0-.51.1-.71.29l-2.2 2.2a15.149 15.149 0 01-6.59-6.59l2.2-2.2c.28-.28.36-.67.25-1.02A11.36 11.36 0 018.5 4c0-.55-.45-1-1-1z"></path>
            </svg>
          </div>
          <div className={styles.telephoneLink}>
            <Link
              to="#"
              onClick={(e) => {
                e.preventDefault()
                window.location = 'tel:96133184'
              }}
            >
              +65 96133184
            </Link>
          </div>
        </div>
      </footer>
    </article>
  )
}

export default RootView
