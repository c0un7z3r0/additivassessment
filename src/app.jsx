import { hot } from 'react-hot-loader/root'
import React from 'react'
import RootView from './components/rootView/rootView'

import './public/css/normalize.css'
import './public/css/scrollbars.scss'
import './public/css/typography.scss'
import './global.scss'

const App = () => {
  return <RootView location={location} />
}

export default hot(App)
